#!/usr/bin/env python
# coding: utf-8

# In[1]:


get_ipython().run_line_magic('matplotlib', 'notebook')
import matplotlib.pyplot as plt
import numpy as np
import ipywidgets as widgets


# In[2]:


# Using interact, we create 2 sliders here for size and step.
# In this case we have size which goes from 1 to 25 with increments
# of 1, and step, which goes from 0.1 to 1 with increments of 0.1
@widgets.interact(size=(1, 25, 1), step=(0.1, 1, 0.1))
def plot(size, step):
    # Create a matplotlib figure
    # We will render everything onto this figure
    fig = plt.figure()
    
    # Add a subplot. You could add multiple subplots but only one will 
    # be shown when using '%matplotlib notebook'
    ax = fig.add_subplot(projection='3d')
    
    # We want X and Y to be the same, so generate a single range
    XY = np.arange(-size, size, step)
    
    # Convert the vectors into a matrix
    X, Y = np.meshgrid(XY, XY)
    
    R = np.sqrt(X**2 + Y**2)
    
    # Plot using sine
    Z = np.sin(R)
    ax.plot_surface(X, Y, Z)
    # Plot using cosine with a Z-offset of 10 to plot above each other
    Z = np.cos(R)
    ax.plot_surface(X, Y, Z + 10)


# In[ ]:




